package sample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by Alexander on 03-11-2015.
 */
public class ChatClient implements Runnable{
    Controller c;
    PrintWriter pw;
    OutputStream os;
    InputStream is;
    String fromServer;

    public ChatClient() {
        try{
            Socket client = new Socket("localhost",12000);
            pw = new PrintWriter(client.getOutputStream(), true);
            fromServer = new String(client.getInputStream().toString());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void run() {
        writeToServer();
        listenToServer();
    }

    public String getFromServer() {
        return fromServer;
    }

    private void writeToServer() {


    }

    private void listenToServer() {




    }
}
