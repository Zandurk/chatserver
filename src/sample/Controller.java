package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


public class Controller {
    public ChatClient cc;
    @FXML
    public Label forsteLabel, andenLabel, tredjeLabel;
    public Button send, quit;
    public TextArea text;

    public void send(){
    text.setEditable(false);
     getTextToServer();
    }

    private String getTextToServer() {
        return text.getText();
    }

    public boolean isSent(){
        if(text.isEditable()){
            return true;
        }else{
            return false;
        }
    }

    public void quit(){
    System.exit(0);
    }

    public void modtagText(){
        if(!forsteLabel.getText().equals(cc.getFromServer())) {
            forsteLabel.setText(cc.getFromServer());
        } else if(!andenLabel.getText().equals(cc.getFromServer())) {
            andenLabel.setText(cc.getFromServer());
        } else {
            tredjeLabel.setText(cc.getFromServer());
        }
    }


}
