package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    ChatClient cc = new ChatClient();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Chat");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
           }

    public static void main(String[] args) {
       new Thread(new ChatServer()).start();
       new Thread(new ClientController()).start();
       new Thread(new ChatClient()).start();
       launch(args);
    }
}
